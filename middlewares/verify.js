const jwt = require('jsonwebtoken');
const User = require('../services/User.service');

module.exports = async (req, res, next) => {
    try {
        if(!req.cookies['user']) {
            res.redirect('/admin/login');
            return
        }

        const user = await User.user(req.cookies['user']);
        if(!user) {
            res.redirect('/admin/login');
            return
        }
        const token = user.token;

        if(token === '') {
            res.redirect('/admin/login');
            return ;
        }

        req.user = jwt.verify(token, process.env.SECRET)

        next();
    }
    catch (e) {
        console.log(e)
        res.send(e);
    }

}