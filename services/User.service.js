const User = require('../models/Users')
const bcrypt = require('bcrypt')

module.exports = new class UserService {
    async  user(username) {
        try {
            return  await  User.findOne({login: username})
        }
        catch (e) {
            console.log(e)
            throw e;
        }
    }

    async changePassword(username, password) {
        try {
            return  await  User.findOneAndUpdate(
                {login: username},
                {password: password}
            )
        }
        catch (e) {
            console.log(e)
            throw e;
        }
    }

    async  setToken(username , token = '') {
        try {
            return  await  User.findOneAndUpdate(
                { login: username},
                { token: token }
            )
        }
        catch (e) {
            console.log(e)
            throw e;
        }
    }
}