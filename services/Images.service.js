const Images = require('../models/Images');
const Projects = require('../models/Projects');

module.exports = new class CountersService {
    async add(images, id){
        try {
            return (await Images.insertMany(images).then( async (items) => {
                await Projects.findByIdAndUpdate(
                    id,
                    { $push: { images: items } }
                    )
            }));
        }
        catch (e) {
            console.error(e);
            throw e;
        }
    }

    async delete(id) {
        try {
            return await Images.findByIdAndDelete(id);
        }
        catch (e) {
            console.error(e);
            throw e;
        }
    }
}