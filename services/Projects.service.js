const Projects = require('../models/Projects')
const CountersService = require('./Counters.service');


module.exports = new class ProjectsService {
   async create(project = {}) {
       try {
            const id = await CountersService.getNextSequenceValue("projectId");
            return await new Projects({
                project: id,
                ...project
            }).save();
       }
       catch (e) {
           console.log(e);
           throw e;
       }
   }

   async delete(id) {
       try {
           return await Projects.findOneAndDelete({_id: id});
       }
       catch (e) {
           throw e;
       }
   }

   async edit(project) {
       try {
           return await Projects.findOneAndUpdate(
        {_id: project.project_id},
        {...project}
        )
       }
       catch (e) {
           console.log(e)
           throw e;
       }
   }

   async getAll() {
       try {
           return await Projects.find()
       }
       catch (e) {
           console.log(e);
           throw e;
       }
   }

    async getEnabled() {
        try {
            return await Projects.find({enabled: true})
        }
        catch (e) {
            console.log(e);
            throw e;
        }
    }

   async getPrev(id) {
       try {
           return await Projects.find().sort({project: -1}).find({project: {$lt: id}, enabled: true})
       }
       catch (e) {
           console.log(e);
           throw e;
       }
   }

   async getNext(id) {
        try {
            return await Projects.find({project: {$gt: id}, enabled: true})
        }
        catch (e) {
            console.log(e);
            throw e;
        }
    }

   async getOne(name) {
        try {
            return await Projects.findOne({browserLink: name}).populate('images')
        }
        catch (e) {
            console.log(e);
            throw e;
        }
   }
}