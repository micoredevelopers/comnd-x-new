require('dotenv').config();
const mongoose = require('mongoose');

module.exports = new class ConnectionService {
    setConnection() {
        mongoose.connect('mongodb://localhost:27017', {
            useNewUrlParser: true,
            user: process.env.DB_USERNAME,
            pass: process.env.DB_PASSWORD,
            dbName: process.env.DB_NAME,
            useUnifiedTopology: true
        })
            .then((db) => {
            console.log('Connection to db is successful');
        })
    }
}