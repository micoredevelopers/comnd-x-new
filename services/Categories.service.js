const Categories = require('../models/Categories');

module.exports = new class CategoriesService {
    async add(name) {
        try {
            return await new Categories({
                category: name
            }).save()
        }
        catch (e) {
            console.log(e)
        }
    }

    async delete(id) {
        try {
            return await Categories.deleteOne({_id: id});
        }
        catch (e){
            console.log(e)
        }
    }

    async edit(category) {
        try {
            return await Categories.updateOne({_id: category.id}, {$set: {category: category.name}})
        }
        catch (e){
            console.error(e)
        }
    }

    async getAll() {
        try {
            return await Categories.find();
        }
        catch (e) {
            console.error(e)
        }
    }
}