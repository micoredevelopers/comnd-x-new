
module.exports = new class AuthorizationService {

    async logout() {
        try {

        }
        catch (e) {

        }
    }

    async setPassword() {
        try {

        }
        catch (e) {

        }
    }

    async validatePassword() {
        try {

        }
        catch (e) {

        }
    }

    async generateJWT() {
        try {
            const today = new Date();
            const expirationDate = new Date(today);
            expirationDate.setDate(today.getDate() + 60);

            return jwt.sign({
                email: this.email,
                id: this._id,
                exp: parseInt(expirationDate.getTime() / 1000, 10),
            }, 'cmndx');
        }
        catch (e) {

        }
    }

    async toAuthJSON() {
        try {
            return {
                _id: this._id,
                email: this.email,
                token: this.generateJWT(),
            };
        }
        catch (e) {

        }
    }
}

