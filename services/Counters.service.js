const Counters = require('../models/Counters');

module.exports = new class CountersService {
     async getNextSequenceValue(sequenceName){
        try {
             const {sequence_value} = await Counters.findOneAndUpdate(
                {_id: sequenceName },
                {$inc:{"sequence_value": 1},
                    new: true
            });

             return sequence_value;
        }
        catch (e) {
            console.log(e);
            throw e;
        }
    }
}