const User = require('../services/User.service')
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');


module.exports = new class AuthController {
    async login(req, res) {
        try {
            const user = await User.user(req.body.login);

            if(!user) {
                res.send({error: 'Неверный пароль'});
                return;
            }

            const password = await bcrypt.compare(req.body.password, user.password);
            if(!password) {
                res.send({error: 'Неверный пароль'});
                return;
            }

            //create and assign a token
            const token = jwt.sign({_id: user._id}, process.env.SECRET, {
                expiresIn: '90d'
            });

            await User.setToken(user.login, token);

            res.cookie('user', user.login,
                {
                    expires: new Date(
                        Date.now() + 90 * 24 * 60 * 60 * 1000
                    )
                }
            );
            res.send({redirect:  `/admin`})
        }
        catch (e) {
            console.log(e)
            throw e;
        }
    }

    async logout(req, res) {
        try {
            await User.setToken(req.cookies['user']).then(() => {
                res.redirect('/admin/login')
            })
        }
        catch (e) {
            throw e;
        }
    }
}