const ProjectService = require('../services/Projects.service');
const CategoriesService = require('../services/Categories.service');

module.exports = new class ProjectsController {
    async adminPage(req, res) {
        try {
            const categories = await CategoriesService.getAll();
            res.render('admin/addingProject', { title: 'Добавление проекта' , categories})
        }
        catch (e) {
            console.log(e)
        }
    }

    async pageProject(req, res) {
        try {
            const project = await ProjectService.getOne(req.params.name);
            const prev = await ProjectService.getPrev(Number(project.project));
            const next = await ProjectService.getNext(Number(project.project));

            res.render('project', { title: project.header, project , prev, next})
        }
        catch (e) {
            console.log(e)
        }
    }

    async pageProjects(req, res) {
        try {
            const categories = await CategoriesService.getAll();
            const projects = await ProjectService.getEnabled();

            res.render('projects', { title: 'Наши работы', projects, categories})
        }
        catch (e) {
            console.log(e)
        }
    }

    async add(req, res) {
        try {
            const project = req.body.project;
            project.enabled = !!project.enabled;

            if(req.files) {
                for(let key in req.files) {
                    const item = req.files[key] || [{path: ''}];
                    project[key] = item[0].path.split('public')[1];
                }
            }

            await ProjectService.create(project);

            res.redirect('/admin');

        } catch (e) {
            console.log(e)
        }
    }

    async delete(req, res) {
        try {
            await ProjectService.delete(req.body.project_id);
            res.redirect('/admin')
        }
        catch (e) {
            throw e;
        }
    }

    async edit(req, res) {
        try {
            const project = req.body.project;
            project.enabled = !!project.enabled;

            if(req.files) {
                for(let key in req.files) {
                    const item = req.files[key] || [{path: ''}];
                    project[key] = item.path.split('public')[1];
                }
            }
            await ProjectService.edit(project);

            res.redirect(req.get('referer'));
        }
        catch (e) {
            console.log(e)
            throw e;
        }
    }
}