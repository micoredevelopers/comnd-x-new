const mailgun = require("mailgun-js");

module.exports = new class MailController {
    async send(req, res) {
        try {
            const mg = mailgun({apiKey: process.env.API_KEY, domain: process.env.DOMAIN});
            const data = {
                from: `${req.body.name} <me@samples.mailgun.org>`,
                to: 'dmitry.shulyachenko.work@gmail.com, dmitry.shulyachenko.work@gmail.com',
                subject: 'Hello',
                text: 'Testing some Mailgun awesomness!'
            };
            mg.messages().send(data, function (error, body) {
                console.log(body);
            });
        }
        catch (e) {
            console.log(e)
            throw e;
        }
    }
}