const ProjectService = require('../services/Projects.service');
const CategoriesService = require('../services/Categories.service');

module.exports = new class AdminController {
    async indexPage(req, res) {
        try {
            const projects = await ProjectService.getAll();
            res.render('admin/index', { title: 'Админка', projects})
        }
        catch (e) {
            res.send(e)
        }
    }

    async loginPage(req, res) {
        try {
            res.render('admin/login', { title: 'Вход'})
        }
        catch (e) {
            res.send(e)
        }
    }

    async settingsPage(req, res) {
        try {
            res.render('admin/settings', { title: 'Настройки' })
        }
        catch (e) {
            throw e;
        }
    }

    async projectPage(req, res) {
        try {
            const project = await ProjectService.getOne(req.params.name);
            const categories = await CategoriesService.getAll();
            console.log(project)

            res.render('admin/project', { title: project.header, project, categories})
        }
        catch (e) {
            throw e;
        }
    }
}