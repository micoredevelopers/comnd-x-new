const CategoriesService = require('../services/Categories.service');

module.exports = new class CategoriesController {
    async page(req, res) {
        try {
            const categories = await CategoriesService.getAll();
            res.render('admin/categories', {title: 'Категории', categories})
        }
        catch (e) {
            res.send(e)
        }
    }

    async add(req, res) {
        try {
            await CategoriesService.add(req.body.category);
            res.redirect('/admin/categories');
        }
        catch (e) {
            res.send(e)
        }
    }

    async delete(req, res) {
        try {
            await CategoriesService.delete(req.body.category_id);
            res.redirect('/admin/categories');
        }
        catch (e) {
            res.send(e);
        }
    }

    async edit(req, res) {
        try {
            const category = {
                id: req.body.category_id,
                name: req.body.category
            }
            await CategoriesService.edit(category);
            res.redirect('/admin/categories');
        }
        catch (e) {
            res.send(e);
        }
    }
}