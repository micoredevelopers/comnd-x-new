const User = require('../services/User.service')
const bcrypt = require('bcrypt')

module.exports = new class SettingsController {
    async changePassword(req, res) {
        try {
            const password = req.body.password;

            if(password && (password.new === password.confirmP)) {
                await User.changePassword(req.cookies['user'], await bcrypt.hash(password.new, 10)).then(() => {
                    res.send({success: 'Пароль изменён'})
                    return;
                })
            }

            res.send({error: 'Пароли не совпадают.'})
        }
        catch (err) {
            res.send(err)
        }
    }
}