const ImagesService = require('../services/Images.service')

module.exports = new class ProjectsController {
    async add(req, res) {
        try {
            if(!req.files) {
                return;
            }

            const files = req.files.map(file => {
                const item = file || [{path: ''}];
                return {
                    url: item.path.split('public')[1],
                    project: req.body.project_id
                }
            })

            await ImagesService.add(files, req.body.project_id);

            res.redirect(req.get('referer'));
        }
        catch (e) {
            console.error(e)
            throw e;
        }
    }

    async delete(req, res) {
        try {
            await ImagesService.delete(req.body.photoId)
            res.redirect('/admin')
        }
        catch (e) {
            console.error(e);
            throw e;
        }
    }
}