const express = require('express');
const router = express.Router();
const uploads = require('../middlewares/uploads');
const mustBeAuth = require('../middlewares/verify');

// Controllers
const adminController = require('../Controllers/Admin.controller');
const ProjectsController = require('../Controllers/Projects.controller');
const CategoriesController = require('../Controllers/Categories.controller');
const AuthorizationController = require('../Controllers/Authorization.controller');
const SettingsController = require('../Controllers/Settings.controller');
const ImagesController = require('../Controllers/Images.controller');
const MailController = require('../Controllers/Mail.controller');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Веб-студия Command+X Одесса - Создание сайтов любой сложности'});
});

// GET about page.
router.get('/about', function(req, res) {
  res.render('about', { title: 'О Нас'});
});

// GET callback page.
router.get('/callback', function(req, res) {
  res.render('callback', { title: 'Связь с нами'});
});

// GET logo page.
router.get('/logo', function(req, res) {
  res.render('logo', { title: 'Лого'});
});

// GET naming page.
router.get('/naming', function(req, res) {
  res.render('naming', { title: 'Нейминг'});
});

// GET sites page.
router.get('/sites', function(req, res) {
  res.render('sites', { title: 'Сайты'});
});

// GET projects page.
router.get('/projects', ProjectsController.pageProjects);

// GET project page.
router.get('/projects/:name', ProjectsController.pageProject);

// GET services page.
router.get('/services', function(req, res) {
  res.render('services', { title: 'Услуги'});
});

// GET admin home
router.get('/admin', mustBeAuth , adminController.indexPage);

// GET admin/projects/add
router.get('/admin/projects/add', mustBeAuth, ProjectsController.adminPage);

// POST admin/projects/add
router.post('/admin/projects/add', mustBeAuth, uploads.fields([{name: "footerImg"}, {name: "headerImg"}]), ProjectsController.add)

// POST admin/projects/delete
router.post('/admin/projects/delete', mustBeAuth, ProjectsController.delete);

// POST admin/projects/edit
router.post('/admin/projects/edit', mustBeAuth, uploads.fields([{name: "footerImg"}, {name: "headerImg"}]), ProjectsController.edit);

// POST admin/project/photos
router.post('/admin/project/photos', mustBeAuth, uploads.array('projectPhotos'), ImagesController.add)

// POST admin/project/photo/delete
router.post('/admin/project/photo/delete', mustBeAuth, ImagesController.delete)

// GET admin/projects/name
router.get('/admin/projects/:name', mustBeAuth, adminController.projectPage);

// GET admin/categories
router.get('/admin/categories', mustBeAuth, CategoriesController.page);

// POST admin/categories/add
router.post('/admin/categories/add', mustBeAuth, CategoriesController.add);

// POST admin/categories/delete
router.post('/admin/categories/delete',mustBeAuth, CategoriesController.delete);

// POST admin/categories/edit
router.post('/admin/categories/edit',mustBeAuth, CategoriesController.edit)

// GET admin/settings
router.get('/admin/settings',mustBeAuth, adminController.settingsPage)

// POST admin/password
router.post('/admin/password', mustBeAuth, SettingsController.changePassword)

// GET admin/login
router.get('/admin/login', adminController.loginPage)

// POST admin/login
router.post('/admin/login', AuthorizationController.login);

// POST admin/logout
router.post('/admin/logout', mustBeAuth, AuthorizationController.logout);

// POST /callback/mail
router.post('/callback', MailController.send)

// GET error page
router.get('*', function(req, res) {
  res.render('error', { title: 'Ошибка 404'});
});


module.exports = router;
