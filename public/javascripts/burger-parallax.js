export default function setParallax() {
    const wrapper = $('.main .main-section');

    wrapper.on('mousemove', function (e) {
        let x = e.clientX / wrapper.width();
        let y = e.clientY / wrapper.height();

        setCoords('#head', 5.5, x, y)
        setCoords('#center', 6, x, y)
        setCoords('#meats', 7, x, y)
        setCoords('#salat', 8, x, y)
        setCoords('#cheese', 9, x, y)
        setCoords('#lag', 10, x, y)
    })

    function  setCoords(selector, cof, x, y) {
        $(selector).css('transform', 'translate(-' + x * (cof * 10)  + 'px, -' + y * (cof * 5) + 'px)')
    }
}
