import setParallax from "./burger-parallax";

let buttonTimeout;

const convertImages = (query, callback) => {
    const images = document.querySelectorAll(query)

    images.forEach(image => {
        fetch(image.src)
            .then(res => res.text())
            .then(data => {
                const parser = new DOMParser()
                const svg = parser.parseFromString(data, 'image/svg+xml').querySelector('svg')

                if (svg) {
                    if (image.id) svg.id = image.id
                    if (image.className) svg.classList = image.classList
                    image.parentNode.replaceChild(svg, image)
                }
            })
            .then(callback)
            .catch(error => console.error(error))
    })
}

convertImages('.svg', function () {
    console.log('success')
});

if($('main').hasClass('main') && window.innerWidth > 992) {
    convertImages('.burger-svg', function () {
        setParallax();
        $('.burger-svg').addClass('active');
        setTimeout(() => {
            $('.burger-svg').removeClass('active');
        }, 2000)
        $('.main-section__img').css('opacity', '1')
    })
}

function btnEvent() {
    const elem = $(this);

    const letters = elem.find('span');
    let time = 0;
    letters.each((index, item) => {
        const delay = index * 1;
        time = `${delay}00`;
        $(item).attr("style",`animation-delay: ${delay < 10 ? '.' + delay : delay.toString()[0] + '.' + delay.toString()[1]}s`);
    })
    elem.removeClass('active')

    if(!elem.hasClass('active')) {
        elem.addClass('active');
    }
    elem.off('mouseenter');

    clearTimeout(buttonTimeout);
    buttonTimeout = setTimeout(function () {
        elem.removeClass('active')
        elem.on('mouseenter', btnEvent)
    }, 400 + Number(time));
}

$('.default-btn').on('mouseenter', btnEvent)

$('input[type="phone"]').mask('+38 (999) 999 99 99', {placeholder: "+38 (0ХХ) XХХ ХХ XX" })
