function setHeaderColor() {
    const header = $('header');
    const main = $('main');
    const logo = $('.header-logo');
    const menu = $('.burger-menu');

    if(main.hasClass('main') || main.attr('id') === 'project-main') {
        menu.find('span').css('backgroundColor', '#fff');
        logo.css('backgroundColor', 'unset')
    }
}

setHeaderColor()