import $ from 'jquery';

$('.login-form, .password-form').on('submit', function (e) {
    e.preventDefault();
    const form = $(e.target);

    $.ajax({
        method: "POST",
        url: form.attr('action'),
        data: form.serialize(),
        success: function (res) {
            $('.error-notification').remove();
            $('.success-notification').remove();

            if(res.error) {
                $('body').append(`<div class="error-notification">${res.error}</div>`)
            } else  {
                if(res.success) {
                    $('body').append(`<div class="success-notification">${res.success}</div>`)
                }
                if(res.redirect) {
                    window.location.replace(res.redirect)
                }
            }

            $('.error-notification, .success-notification').on('click', function (e) {
                $(e.target).remove();
            })
        }
    })
})