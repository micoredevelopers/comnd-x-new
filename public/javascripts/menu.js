let timeout;
let navtimeout;
let overlays;

function setNavDelay() {
    const items = $('.header-nav__item');

    items.each((index, item) => {
        const delay = index * 1;
        $(item).css('transitionDelay', `${delay < 10 ? '.' + delay : delay.toString()[0] + '.' + delay.toString()[1]}s`);
    })
}

setNavDelay()

function menuEvent() {
    const item = $(this);
    clearTimeout(timeout)
    if(!item.hasClass('is-open') && !item.hasClass('active')) {
        item.addClass('active');
        item.off('mouseenter');
        timeout = setTimeout(function () {
            item.removeClass('active');
            item.on('mouseenter', menuEvent);
        }, 1400)
    }
}

$('.burger-menu').on('mouseenter', menuEvent)

const ease = {
    cubicOut: (t) => {
        const f = t - 1.0;
        return f * f * f + 1.0;
    },
    cubicInOut: (t) => {
        return t < 0.5
            ? 4.0 * t * t * t
            : 0.5 * Math.pow(2.0 * t - 2.0, 3.0) + 1.0;
    }
}

class ShapeOverlays {
    constructor(elm) {
        this.elm = elm;
        this.path = elm.querySelectorAll('path');
        this.numPoints = 2;
        this.duration = 600;
        this.delayPointsArray = [];
        this.delayPointsMax = 0;
        this.delayPerPath = 200;
        this.timeStart = Date.now();
        this.isOpened = false;
        this.isAnimating = false;
    }
    toggle() {
        this.isAnimating = true;
        for (let i = 0; i < this.numPoints; i++) {
            this.delayPointsArray[i] = 0;
        }
        if (this.isOpened === false) {
            this.open();
        } else {
            this.close();
        }
    }
    open() {
        this.isOpened = true;
        this.elm.classList.add('is-opened');
        this.timeStart = Date.now();
        this.renderLoop();
    }
    close() {
        this.isOpened = false;
        this.elm.classList.remove('is-opened');
        this.timeStart = Date.now();
        this.renderLoop();
    }
    updatePath(time) {
        const points = [];
        for (let i = 0; i < this.numPoints; i++) {
            const thisEase = this.isOpened ?
                (i === 1) ? ease.cubicOut : ease.cubicInOut:
                (i === 1) ? ease.cubicInOut : ease.cubicOut;
            points[i] = thisEase(Math.min(Math.max(time - this.delayPointsArray[i], 0) / this.duration, 1)) * 100
        }

        let str = '';
        str += (this.isOpened) ? `M 0 0 V ${points[0]} ` : `M 0 ${points[0]} `;
        for (let i = 0; i < this.numPoints - 1; i++) {
            const p = (i + 1) / (this.numPoints - 1) * 100;
            const cp = p - (1 / (this.numPoints - 1) * 100) / 2;
            str += `C ${cp} ${points[i]} ${cp} ${points[i + 1]} ${p} ${points[i + 1]} `;
        }
        str += (this.isOpened) ? `V 0 H 0` : `V 100 H 0`;
        return str;
    }
    render() {
        if (this.isOpened) {
            for (let i = 0; i < this.path.length; i++) {
                this.path[i].setAttribute('d', this.updatePath(Date.now() - (this.timeStart + this.delayPerPath * i)));
            }
        } else {
            for (let i = 0; i < this.path.length; i++) {
                this.path[i].setAttribute('d', this.updatePath(Date.now() - (this.timeStart + this.delayPerPath * (this.path.length - i - 1))));
            }
        }
    }
    renderLoop() {
        this.render();
        if (Date.now() - this.timeStart < this.duration + this.delayPerPath * (this.path.length - 1) + this.delayPointsMax) {
            requestAnimationFrame(() => {
                this.renderLoop();
            });
        }
        else {
            this.isAnimating = false;
        }
    }
}

(function() {
    const elmOverlay = document.querySelector('.shape-overlays');
    const overlay = new ShapeOverlays(elmOverlay);

    $('.burger-menu').on('click', function () {
        if (overlay.isAnimating) {
            return false;
        }
        const item = $(this);
        item.toggleClass('is-open');
        item.removeClass('active')
        item.removeClass('isAnimated')
        $('.header-logo').toggleClass('active');
        clearTimeout(navtimeout)
        clearTimeout(overlays)
        if(item.hasClass('is-open')) {
            overlay.toggle();
            navtimeout = setTimeout(function () {
                $('.header-nav').addClass('active');
            }, 500)
        } else {
            overlays = setTimeout(function () {
                overlay.toggle();
            }, 200)
            $('.header-nav').removeClass('active');
            item.on('mouseenter', menuEvent)
        }

        clearTimeout(timeout);
    });
}());