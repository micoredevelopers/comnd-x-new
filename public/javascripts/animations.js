import AOS from 'aos';
let menuTimeout;

AOS.init({
    once: true,
    easing: 'default-cubic',
    offset: 0
});

function animateMenu() {
    const menu = $('.burger-menu');
    menu.addClass('isAnimated');
    clearTimeout(menuTimeout)
    menuTimeout = setTimeout(function () {
        menu.removeClass('isAnimated')
    }, 3000)
}

function refreshAnimation() {
    $(document).on('scroll', function () {
        if(pageYOffset > 30) {
            AOS.refresh({
                once: true,
                easing: 'default-cubic',
                offset: 0
            });
        }
    })
}

refreshAnimation()

animateMenu()