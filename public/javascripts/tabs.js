function setTabToggler(selector) {
    $(selector).on('click', function () {
        const item = $(this);

        if(!item.hasClass('active')) {
            $(selector).removeClass('active');
            $(this).addClass('active')
            toggleTabs('.project', $(this).attr('data-sort'))
        }
    })
}

function toggleTabs(selector, filter) {
    const tabs = $(selector);
    tabs.removeClass('active')
    setTimeout(function () {
        tabs.removeClass('d-block')
    }, 20)

    setTimeout(function () {
        if(!filter || filter === 'all') {
            tabs.addClass('d-block')
            setTimeout(function () {
                tabs.addClass('active')
            }, 20)
        } else {
            $(`.project[data-sort="${filter}"]`).addClass('d-block')

            setTimeout(function () {
                $(`.project[data-sort="${filter}"]`).addClass('active')
            }, 20)
        }
    }, 40)
}

toggleTabs('.project')

setTabToggler('.projects-nav .tab');