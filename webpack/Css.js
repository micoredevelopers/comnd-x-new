const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = (path) => new MiniCssExtractPlugin({ filename: path })
