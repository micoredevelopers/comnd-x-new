const Mongoose = require('mongoose');

const Users = new Mongoose.Schema({
    login: String,
    password: String,
    token: String
})

module.exports = Mongoose.model('Users', Users);