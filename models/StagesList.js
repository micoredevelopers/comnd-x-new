const Mongoose = require('mongoose');

const StagesList = new Mongoose.Schema({
    title: {
        type: String
    },
    url: {
        type: String
    }
})

module.exports = Mongoose.model('StagesList', StagesList);