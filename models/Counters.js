const Mongoose = require('mongoose');

const Counters = new Mongoose.Schema({
    _id : {
        type: String
    },
    sequence_value: {
        type: Number
    }
})

module.exports = Mongoose.model('Counters', Counters);