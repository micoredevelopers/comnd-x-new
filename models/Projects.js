const Mongoose = require('mongoose');

const Projects = new Mongoose.Schema({
    project: {
        type: Number,
        unique: true
    },
    header: {
        type: String
    },
    description: {
        type: String
    },
    headerInfo: {
        type: String
    },
    descriptionInfo: {
        type: String
    },
    officialSite: {
        type: String
    },
    metaTitle: {
        type: String
    },
    metaDescription: {
        type: String
    },
    metaKeywords: {
        type: String
    },
    browserLink: {
        type: String
    },
    enabled: {
        type: Boolean
    },
    footerImg: {
        type: String
    },
    headerImg: {
        type: String
    },
    bodyClass: {
        type: String
    },
    category: {
        type: Mongoose.Schema.Types.ObjectId,
        ref: 'Categories'
    },
    images: [
        {
            type: Mongoose.Schema.Types.ObjectId,
            ref: 'Images'
        }
    ]
})

module.exports = Mongoose.model('Projects', Projects);