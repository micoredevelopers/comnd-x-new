const Mongoose = require('mongoose');

const Stages = new Mongoose.Schema({
    header: {
        type: String
    },
    url: {
        type: String
    },
    project: {
        type: Mongoose.Schema.Types.ObjectId,
        ref: 'Projects'
    }
})

module.exports = Mongoose.model('Stages', Stages);