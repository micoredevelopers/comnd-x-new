const Mongoose = require('mongoose');

const Images = new Mongoose.Schema({
    url: {
        type: String
    },
    project: {
        type: Mongoose.Schema.Types.ObjectId,
        ref: 'Projects'
    }
})

module.exports = Mongoose.model('Images', Images);