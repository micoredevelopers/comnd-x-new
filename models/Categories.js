const Mongoose = require('mongoose');

const Categories = new Mongoose.Schema({
    category: {
        type: String,
        required: true
    }
})

module.exports = Mongoose.model('Categories', Categories);