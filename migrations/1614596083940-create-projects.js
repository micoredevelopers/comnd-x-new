const Projects = require('../models/Projects')

async function up () {
    Projects.createIndex( {index: 1}, {unique: true})
}

/**
 * Make any changes that UNDO the up function side effects here (if possible)
 */
async function down () {

}

module.exports = { up, down };
