const CssPlugins = require('./webpack/Css')
const SassPlugins = require('./webpack/Sass')

module.exports = [{
    mode: 'production',
    output: { filename: './js/bundle.js' },
    entry: ['./public/javascripts/index.js', './public/stylesheets/main/main.scss'],
    module: {
        rules: [
            {
                test: /\.js$/,
                use: 'babel-loader',
                exclude: [
                    /node_modules/
                ]
            },
            SassPlugins('../public/stylesheets/main/main.scss')
        ]
    },
    plugins: [  // Array of plugins to apply to build chunk
        CssPlugins('css/style.bundle.css')
    ]
}, {
    mode: 'production',
    output: { filename: './js/admin/bundle.js' },
    entry: ['./public/javascripts/admin/index.js', './public/stylesheets/admin/main.scss'],
    module: {
        rules: [
            {
                test: /\.js$/,
                use: 'babel-loader',
                exclude: [
                    /node_modules/
                ]
            },
            SassPlugins('../public/stylesheets/admin/main.scss')
        ]
    },
    plugins: [  // Array of plugins to apply to build chunk
        CssPlugins('css/admin/style.bundle.css')
    ]
}];